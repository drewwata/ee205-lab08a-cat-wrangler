///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// A generic list class.  May be used as a base class for a Doubly Linked List
///
/// @author Drew Watanabe <@todo drewwata@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   14_Apr_2021 
/////////////////////////////////////////////////////////////////////////////////


#include <iostream>

#include "list.hpp"


const bool DoubleLinkedList::empty() const {
   //std::cout<<"In empty()" << std::endl;
   if (count == 0) {
      return true;
   } else {
      return false;
   }
}

void DoubleLinkedList::push_front( Node* newNode ) {
   //std::cout<<"In pushfront" << std::endl;

   if(newNode == nullptr){
      return;
   }
   if(empty()){
      count ++;
      newNode-> next = nullptr;
      newNode-> prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   else{
   count ++;
   newNode->next = head;
   newNode->prev = nullptr;
   head->prev = newNode;
   head = newNode;

   }
}

Node* DoubleLinkedList::pop_front() {
   //std::cout<<"In popfront" << std::endl;

   if(head == nullptr)
      return nullptr;

   Node* temp = head;
   head = temp->next;
   head->prev = nullptr;
   count--;
   return temp;

}

Node* DoubleLinkedList::get_first() const {
   //std::cout<<"In getfirst" << std::endl;

return head;

}

Node* DoubleLinkedList::get_next(const Node* currentNode ) const {
   //std::cout<<"In getnext" << std::endl;


if(currentNode == nullptr)
   return nullptr;

return currentNode->next;

}

   void DoubleLinkedList::push_back( Node* newNode ){
   //std::cout<<"In pushback" << std::endl;

  if(newNode == nullptr){
      return;
   }
   if(empty()){
      count ++;
      newNode-> next = nullptr;
      newNode-> prev = nullptr;
      tail = newNode;
      head = newNode;
   }
   else{
   count ++;
   newNode->next = nullptr;
   newNode->prev = tail;
   tail->next = newNode;
   tail = newNode;

   } 

   }

   Node* DoubleLinkedList::pop_back(){
   //std::cout<<"In popback" << std::endl;

   Node* temp = tail;

   if(size() == 0){
      return temp;
   }
   else if( size() == 1){
   head = nullptr;
   tail = nullptr;
   count--;
   }
   else if( size() == 2){
   head->next = nullptr;
   tail = head;
   count--;
   }
   else{
   tail = temp->prev;
   tail->next = nullptr;
   count --;
   }
   return temp;

   }

   Node* DoubleLinkedList::get_last() const{
   //std::cout<<"In getlast" << std::endl;

   return tail;

   }

   Node* DoubleLinkedList::get_prev( const Node* currentNode ) const{
   //std::cout<<"In getprev" << std::endl;

   if(currentNode == nullptr)
      return nullptr;

   return currentNode->prev;

   }
